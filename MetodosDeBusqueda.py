'''
Created on 4 dic. 2020

@author: Ruby
'''

class MetodosBusqueda:
    def __init__(self):
        pass
    
    
    class BusquedaBinaria:
        
        def busqueda (self, numeros, valorBuscado):
            
            inicio=0
            final=len(numeros)-1
            x=0

            while inicio<=final:
                puntero=(inicio+final)//2
                if (valorBuscado==numeros[puntero]):
                    x=1
                    break
                elif(valorBuscado>numeros[puntero]):
                    inicio=puntero+1
                else:
                    final=puntero-1

            return x==1

mb = MetodosBusqueda()        
numeros = [1,4,6,8,19,23]
buscado = int(input("Ingresa el valor a buscar: "))
print(f"Vector: {numeros}")

res = mb.BusquedaBinaria.busqueda(mb,numeros, buscado)

if(res == True):
    print("Se encontro el dato")
else:
    print("No se encontro")
    

